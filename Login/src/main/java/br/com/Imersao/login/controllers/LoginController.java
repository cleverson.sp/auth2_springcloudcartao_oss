package br.com.Imersao.login.controllers;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.Imersao.login.models.Login;
import br.com.Imersao.login.services.LoginService;

@RestController
@RequestMapping("/login")
public class LoginController {
	
	@Autowired
	LoginService loginService;
	
	@PostMapping("/criar")
	public Login criar(String cpf, String senha) {
		return loginService.criar(cpf, senha);
	}
	
	  @GetMapping("/me")
	  public Map<String, String> validar(Principal principal) {
	    Map<String, String> map = new HashMap<>();

	    map.put("name", principal.getName());
	    
	    return map;
	  }

}
