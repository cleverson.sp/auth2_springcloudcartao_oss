package br.com.Imersao.Cliente.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.Imersao.Cliente.models.Cliente;
import br.com.Imersao.Cliente.repositories.ClienteRepository;
import br.com.Imersao.Cliente.repositories.LoginClient;

@Service
public class ClienteService {
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private LoginClient LoginClient;

	public Cliente criar(Cliente cliente, String cpf, String senha) {

		cliente.setId(cpf);
		
		LoginClient.criar(cpf, senha);
		
		return clienteRepository.save(cliente);
	}

	public Optional<Cliente> buscar(String id) {
		return clienteRepository.findById(id);
	}

}
