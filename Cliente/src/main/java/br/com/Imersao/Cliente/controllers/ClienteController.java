package br.com.Imersao.Cliente.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.Imersao.Cliente.models.Cliente;
import br.com.Imersao.Cliente.services.ClienteService;
//import br.itau.com.cartoes.dtos.Login;
//import br.itau.com.cartoes.dtos.RespostaLogin;
//import br.itau.com.cartoes.security.JwtTokenProvider;

@RestController
public class ClienteController {
  @Autowired
  private ClienteService clienteService;
  
//  @Autowired
//  private JwtTokenProvider tokenProvider;
  
  @PostMapping("/cliente")
  @ResponseStatus(code = HttpStatus.CREATED)
  public Cliente criar(@Valid @RequestBody Cliente cliente, @RequestParam String cpf, @RequestParam String senha) {
    return clienteService.criar(cliente, cpf, senha);
  }
  
  @GetMapping("/cliente/{id}")
  public Optional<Cliente> buscar(@PathVariable String id) {
    return clienteService.buscar(id);
  }
  
}
